package database

import (
	"time"

	"github.com/jinzhu/gorm"
)

type Artist struct {
	gorm.Model

	Name string
	Role string
}
type Movie struct {
	gorm.Model

	Title       string
	ReleaseDate time.Time
	CastAndCrew []Artist
}

type User struct {
	gorm.Model

	Name    string
	Contact string
}
type Reviews struct {
	gorm.Model

	MovieDetails Movie
	Reviwer      User
	Comment      string
}
