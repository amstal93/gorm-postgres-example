package database

import (
	"fmt"
	"log"
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

// DBClient is a custom database client object type
type DBClient struct {
	Client *gorm.DB
	Logger *log.Logger
}

var (
	db  *gorm.DB
	err error
)

// NewDBClient creates database client object
func NewDBClient(logger *log.Logger) (*DBClient, error) {
	db, err = createDBClient()
	if err != nil {
		return nil, err
	}

	return &DBClient{
		Client: db,
		Logger: logger,
	}, nil
}

// create creates the GORM DB object
func createDBClient() (*gorm.DB, error) {
	host := os.Getenv("PSQL_HOST")
	port := os.Getenv("PSQL_PORT")
	password := os.Getenv("POSTGRES_PASSWORD")
	fmt.Println(password)
	db_string := fmt.Sprintf("host=%s port=%s user=postgres password=%s dbname=postgres sslmode=disable", host, port, password)
	db, err = gorm.Open("postgres", db_string)
	if err != nil {
		return nil, fmt.Errorf("Failed at gorm connection open: %v", err)
	}

	if err = db.DB().Ping(); err != nil {
		return db, fmt.Errorf("Failed at db ping: %v", err)
	}
	return db, nil
}
